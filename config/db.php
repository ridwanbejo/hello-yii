<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=soccerdb',
    'username' => 'root',
    'password' => 'root',
    'charset' => 'utf8',
];
