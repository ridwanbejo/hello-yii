<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

class HelloAssets extends Controller
{
   /*
        - mendaftarkan file statis
        - menempelkan potongan kode statis
   */

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        echo "Hello World";
    }
}
